import axios from "axios";

export function setTokenHeader(token) {
  if (token) {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
}

export function apiCall(method, path, data) {
  return new Promise((resolve, reject) => {
    return axios[method.toLowerCase()](
        `http://rupe-be.pheonixsolutions.com:4001/api/${path}`,
        data
      )
      .then((res) => {
        return resolve(res);
      })
      .catch((err) => {
        if (typeof err.response === "undefined")
          return reject({message:err.message});
        else
          return reject(err.response.data);
      });
  });
}
