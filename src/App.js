import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch,Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Cookies from "js-cookie";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import Login from "./components/login";
import Register from "./components/register";
import Home from "./components/home";
import SellorBuy from "./components/sellorbuy";
import Deals from "./components/deals";
import Add from "./components/add";
import Withdraw from "./components/withdraw";
import Wallet from "./components/wallet";
import Hold from "./components/hold";
import Sidenav from "./components/sidenav";
import Navigationbar from "./components/navbar";
import Account from "./components/account";
import LogRegBG from "./components/logregbg";
import PageNotFound from "./components/pagenotfound";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  protect = async () => {
    let accessToken = Cookies.get("access");
    let refreshToken = Cookies.get("refresh");
    if (!accessToken) {
      window.location = "/login";
    } 
  };

  render() {
    return (
      <Router>
        <Navigationbar />
        <Sidenav /> 
          {this.props.auth?<Switch>
        <Route
            path="/home"
            exact
            render={() => <Home protect={this.protect} />}
          />
          <Route
            path="/sellbuy"
            exact
            render={() => <SellorBuy protect={this.protect} />}
          />
          <Route
            path="/deals"
            exact
            render={() => <Deals protect={this.protect} />}
          />
          <Route
            path="/add"
            exact
            render={() => <Add protect={this.protect} />}
          />
          <Route
            path="/withdraw"
            exact
            render={() => <Withdraw protect={this.protect} />}
          />
          <Route
            path="/wallet"
            exact
            render={() => <Wallet protect={this.protect} />}
          />
          <Route
            path="/hold"
            exact
            render={() => <Hold protect={this.protect} />}
          />
          <Route
          path="/login"
          exact
          render={() => <Redirect to="/home" />} 
        />
         <Route
            path="/account"
            exact
            render={() => <Account protect={this.protect} />}
          />
        <Route path="*" render={() => <PageNotFound />}/>
        </Switch>: <Switch><Route
          path="/login"
          exact
          render={() => (
            <div>
              <LogRegBG /> <Login protect={this.protect} />
            </div>
          )}
        />
        <Route
          path="/register"
          exact
          render={() => (
            <div>
              <LogRegBG /> <Register protect={this.protect} />
            </div>
          )}
        />
        <Route path="*" render={() => <Redirect to="/login" />} />
        </Switch>}
      </Router>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    auth: state.currentUser.isAuthenticated
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (user) => {
      dispatch({ type: "AUTHENTICATE_USER", userData: user });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
