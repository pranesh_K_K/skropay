import React, { Component } from "react";
import axios from "axios";
import PasswordStrengthBar from "react-password-strength-bar";
import Cookies from "js-cookie";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";

import "./register.css";
import logo from "../assets/logo.png";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: "",
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      confirmpass: "",
      imageUrl:"",
      profile_img:"",
      error: false,
      warning: "No warning yet",
      errorwith: [],
    };
    this.upload=this.upload.bind(this);
  }

  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  upload(e) {
    const [file] = e.target.files
    if (file) {
     this.setState({imageUrl:URL.createObjectURL(file),profile_img:e.target.files[0]})
    }
  }
  register = (e) => {
    e.preventDefault();

    if (this.state.password !== this.state.confirmpass) {
      this.setState({ error: true, warning: "The passwords don't match." });
      return;
    }
    if (this.state.password.length < 8) {
      this.setState({
        error: true,
        warning: "Password should be of atleast 8 characters.",
      });
      return;
    }

    this.setState({ error: false });
    var formData = new FormData();
    formData.append("phone_number", this.state.phone);
    formData.append("first_name", this.state.firstname);
    formData.append("last_name", this.state.lastname);
    formData.append("email", this.state.email);
    formData.append("password", this.state.password);
    formData.append("username", this.state.firstname +" "+ this.state.lastname);
    formData.append("profile_image", this.state.profile_img);

    axios
      .post(
        "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/register/",
        formData,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      )
      .then((response) => {
        //handle success
        window.location="/login";
      })
      .catch((error) => {
        //handle error
        this.setState({
          errorwith: Object.keys(error.response.data),
          error: true,
          warning: "User Already Exists.",
        });
      });
  };
  render() {
    return (
      <div className="register">
        <div className="logreglogo">
          <img src={logo} alt="logo" height={40} />
        </div>
        <div className="reg-step-1">
          <div className="login-title">Create your Skropay Account</div>
          <form onSubmit={this.register} className="maxlogregwidth">
            <div>
              <input
                className="skro-input hwl"
                type="text"
                name="firstname"
                placeholder="First Name"
                value={this.state.firstname}
                onChange={this.handleInput}
                required
              />
              <input
                className="skro-input hwr"
                type="text"
                name="lastname"
                placeholder="Last Name"
                value={this.state.lastname}
                onChange={this.handleInput}
                required
              />
            </div>
            <PhoneInput
              className={
                "skro-input fw" +
                (this.state.errorwith.includes("phone_number")
                  ? " skro-danger-input"
                  : "")
              }
              name="phone"
              placeholder="Enter phone number"
              value={this.state.phone}
              onChange={(x) => this.setState({ phone: x })}
              required
            />
            <input
              className={
                "skro-input fw" +
                (this.state.errorwith.includes("email")
                  ? " skro-danger-input"
                  : "")
              }
              id="email"
              type="email"
              name="email"
              placeholder="Email Address"
              value={this.state.email}
              onChange={this.handleInput}
              pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
              required
            />
            <input
              className="skro-input fw"
              type="password"
              name="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleInput}
              autocomplete="new-password"
              required
            />
            <PasswordStrengthBar password={this.state.password} />
            <input
              className="skro-input fw"
              type="password"
              name="confirmpass"
              placeholder="Confirm Password"
              value={this.state.confirmpass}
              onChange={this.handleInput}
              required
            />
            <div style={{textAlign:"center"}}>
                 <span class="dot" style={{backgroundSize:"100px 100px",backgroundPosition:"center",backgroundImage:this.state.imageUrl?`url(${this.state.imageUrl})`:false}}></span>
            </div>
            <div className="button-wrapper">
              <span className="label">
                Profile Upload
              </span>
                <input type="file" name="upload" id="upload" onChange={this.upload} className="upload-box" placeholder="Profile Upload"/>
            </div>
            <div className="space-1"></div>
            <label className="tnc" htmlFor="tnc">
              I accept to the Terms and Conditions and Privacy Policy
              <input type="checkbox" name="tnc" value="tnc" id="tnc" required />
              <span className="checkmark"></span>
            </label>

            <p
              className="warning noselect"
              style={{ color: this.state.error ? "red" : "white" }}
            >
              {this.state.warning}
            </p>
            <button type="submit" className="skro-btn-3">
              Next
            </button>
          </form>
          <div className="alignc mt20">
            Already a User? <Link to="/login">Login</Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.userToken,
  };
};

export default connect(mapStateToProps)(Register);
