import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

import './add.css';

export const AddMoney = class AddMoney extends Component {
    render() {
        return (
            <div className="add">
                <div className="withdraw-form">
                    <div className="withdraw-input">
                        <input type="text" value='₹' className="curr-symbol" disabled />
                        <input type="number" placeholder="Amount" min="0" className="withdraw-amount" />
                    </div>
                    <button className="skro-btn-2">Proceed</button>
                </div>
                <p className="add-label">🧧 Money will be added to your wallet</p>
            </div>
        )
    }
}

class Add extends Component {
    render() {
        return (
            this.props.auth ?
                (<div className="shift">
                    <h1>Add Money</h1>
                    <br />
                    <AddMoney />
                </div>
                ) :
                <Redirect to={{ pathname: '/login', state: { from: "/add" } }} />)
    }
}

const mapStateToProps = (state) => {
    return {
        auth: !state.currentUser.isAuthenticated,
    }
}

export default connect(mapStateToProps)(Add)