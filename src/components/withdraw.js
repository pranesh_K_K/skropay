import React, { Component } from 'react';
import { connect } from 'react-redux';

import './withdraw.css';

export const WithdrawMoney = class WithdrawMoney extends Component {
    render() {
        return (
            <div className="withdraw">
                <div className="balance">
                    <p className="bal-label">Available Skro Balance</p>
                    <p className="bal">₹2.50</p>
                </div>
                <div className="withdraw-form">
                    <div className="withdraw-input">
                        <input type="text" value='₹' className="curr-symbol" disabled />
                        <input type="number" placeholder="Amount" min="0" className="withdraw-amount" />
                    </div>
                    <button className="skro-btn-2">Withdraw</button>
                </div>
            </div>
        )
    }
}

class Withdraw extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="shift">
                <h1>Withdraw Money</h1>
                <br />
                <WithdrawMoney />
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        user: state.userToken,
    }
}

export default connect(mapStateToProps)(Withdraw)