import React, { Component } from "react";
import { ChatFeed, Message } from "react-chat-ui";
import { connect } from "react-redux";

import { w3cwebsocket as W3CWebSocket } from "websocket";
import "./deals.css";
import icon from "../assets/icon-01.png";
import send from "../assets/icon-02.png";

const client = new W3CWebSocket(
  "ws://skropay-env.ap-south-1.elasticbeanstalk.com/ws/chat/123/"
);

class Deals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [
        new Message({
          id: 1,
          message: "I'm the recipient! (The person you're talking to)",
        }), // Gray bubble
        new Message({ id: 0, message: "I'm you -- the blue bubble!" }), // Blue bubble,
      ],
      chatmode: false,
      chatusers: [
        { user: "Teja Pujari", pending: 0 },
        { user: "Teja Pujari", pending: 3 },
        { user: "Teja Pujari", pending: 0 },
        { user: "Teja Pujari", pending: 2 },
        { user: "Teja Pujari", pending: 6 },
        { user: "Teja Pujari", pending: 0 },
      ],
    };
  }

  async componentDidMount() {
    client.onopen = () => {
      console.log("Websocket Client Connected");
    };

    client.onmessage = message => {
      const dataFromServer = JSON.parse(message.data);
      if(message.username===this.state.username)
      this.setState(prevState => ({
        messages: [...prevState.messages, new Message({ id: 0, message: dataFromServer.message })]
      }))
      else
      this.setState(prevState => ({
        messages: [...prevState.messages, new Message({ id: 1, message: dataFromServer.message })]
      }))
    }

    client.onclose = () => {
      console.log("Closed");
    }
  }

  onMessageSubmitIcon = () => {
    const input = this.message;
    if (!input.value) {
      return false;
    }
    this.pushMessage(input.value);
    input.value = "";
    return true;
  };

  onMessageSubmit(e) {
    e.preventDefault();
    this.onMessageSubmitIcon();
  }

  pushMessage(message) {
    try {
      client.send(JSON.stringify({message, username: "dunfred"}));
    } catch (error){
      console.log(error);
    }
  }

  render() {
    return !this.props.auth ? (
      <div className="deals shift">
        <div className="dealsfirst">
          <div className="selecteduser">
            <div className="seluser">
              <img src={icon} alt="user" height={70} />
              <div className="seluserdetails">
                <p className="selusernamenum">Teja Pujari</p>
                <p className="selusernamenum">+91 90909 90909</p>
                <p className="selusermailloc">tejap@gmail.com</p>
                <p className="selusermailloc">Mumbai, India</p>
              </div>
            </div>
            <div className="seluseractions">
              <p className="reqstatus">Request Sent</p>
              <button className="skro-btn-basic">Chat</button>
            </div>
          </div>

          <div className="chats">
            <p className="chatstitle">Message</p>
            <div className="chatswrapper">
              <div className="chatusers">
                {this.state.chatusers.map((info, index) => (
                  <div className="chatuser" key={"chatuser-"+index}>
                    <img
                      className="chatuserimg"
                      src={icon}
                      alt="usericon"
                      height={50}
                    />
                    <p className="chatusername">{info.user}</p>
                    {info.pending !== 0 && (
                      <p className="chatnotif">{info.pending}</p>
                    )}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="dealssecond">
          <div className="chatbox">
            <div className="cbuser">
              <div className="cbuserdetails">
                <img src={icon} height={50} alt="icon" className="cbuserimg" />
                <div className="cbusername">Teja Pujari</div>
              </div>
              <button className="skro-btn">Pay</button>
            </div>
            <hr />
            <div className="chatinstruc">
              Click on pay if your deal is done
              <br /> It will be debited to escrow wallet with Teja Pujari
            </div>
            <ChatFeed
              messages={this.state.messages} // Array: list of message objects
              hasInputField={false} // Boolean: use our input, or use your own
              showSenderName // show the name of the user who sent the message
              bubblesCentered={false} //Boolean should the bubbles be centered in the feed?
              // JSON: Custom bubble styles
              bubbleStyles={{
                text: {
                  fontSize: 12,
                },
                chatbubble: {
                  borderRadius: 20,
                },
              }}
            />
            <form
              className="cbinputsend"
              onSubmit={(e) => this.onMessageSubmit(e)}
            >
              <input
                ref={(m) => {
                  this.message = m;
                }}
                placeholder="Type a message here"
                className="chatboxinput"
              />
              <img
                onClick={this.onMessageSubmitIcon}
                className="click"
                src={send}
                alt="submit"
                height={40}
              />
            </form>
          </div>
        </div>
      </div>
    ) : null;
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.userToken,
  };
};

export default connect(mapStateToProps)(Deals);
