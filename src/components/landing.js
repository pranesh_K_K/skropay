import React, { Component } from 'react';
import { connect } from 'react-redux';

import './home.css';
import android from '../assets/android.png'
import ios from '../assets/ios.png'

class LandingPage extends Component {
    render() {
        return (
            <div className="lp">
                <div className="headline">
                    <h1>Never pay without using Skropay!</h1>
                    <div>Skropay is a secure transaction platform that completely protects you from being scammed when you want to buy or sell with someone you don't know.</div>
                </div>
                <div className="grayspace">
                    <button className="skro-btn-1">I'm a Seller</button>
                    <button className="skro-btn-1">I'm a Buyer</button>
                </div>
                <div className="download-apps">
                    <a href="https://google.com" className="ios"><img src={android} alt="android-app" /></a>
                    <a href="https://apple.com" className="android"><img src={ios} alt="ios-app" /></a>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        user: state.userToken,
    }
}

export default connect(mapStateToProps)(LandingPage)