import React, { Component } from "react";
import axios from "axios";
import sample from "../assets/icon-07.png";
import search from "../assets/icon-search.svg";
import loading from "../assets/loading.gif";

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      error:false,
      searchresults: [],
      searchinput: "",
    };

    this.wrapperRef = React.createRef();
    this.resultRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  toggle = () => {
    this.setState(({showSuggestions}) => {
      return { showSuggestions: !showSuggestions}
    })
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside = (event) => {
    const expr1 = (this.wrapperRef && this.wrapperRef.current && this.wrapperRef.current.contains(event.target)) || (this.resultRef && this.resultRef.current && this.resultRef.current.contains(event.target))
    const expr2 = (this.wrapperRef && this.wrapperRef.current && !this.wrapperRef.current.contains(event.target)) && (this.resultRef && this.resultRef.current && !this.resultRef.current.contains(event.target))

    if (expr1) {
      this.setState({showSuggestions: true})
    }
    if (expr2) {
      this.setState({showSuggestions: false})
    }
  }
  search = (e) => {
    e.preventDefault();

    this.setState({ error: false });

    axios
      .get(
        "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/users-search-filter/?id="+`${e.target.value}`,
      )
      .then((response) => {
        //handle success
        this.setState({searchresults:response.data.map(data=>{return {id:data.id,name:data.username}})});
      })
      .catch((error) => {
        console.log(error);
        //handle error
        this.setState({
          errorwith: Object.keys(error.response.data),
          error: true,
          warning: "User Already Exists.",
        });
      });
  };
  render() {
    return (
      <div className="searchcompo">
        <div className="searchtitle">
          Start{" "}
          {this.props.mode.substring(0, this.props.mode.length - 2) + "ing"}{" "}
          with Skropay
        </div>
        <div className="searchbox">
          <img src={search} alt="search" height={20} />
          <input
            ref={this.wrapperRef}
            type="text"
            className="search"
            onChange={this.search}
            placeholder="Search with Skropay ID or Phone Number"
          />
        </div>
        {/*Have to impement something which waits for 2s and starts searching Also empty the array once the user types something */}
        {this.state.searchresults.length === 0 &&
          this.state.showSuggestions &&
          this.state.searchinput !== "" && (
            <div className="searchresults">
              <div className="searchloading">
                <img src={loading} alt="loading" height={30} />
              </div>
            </div>
          )}
        {this.state.searchresults.length !== 0 && this.state.showSuggestions && (
          <div className="searchresults" ref={this.resultRef}>
            {this.state.searchresults.map((info, index) => (
              <div className="searchresult" key={"result" + index}>
                <div className="resultuser">
                  <img src={sample} alt="user" height={50} />
                  <p>{info.name}</p>
                </div>
                <div className="resultreq">Send Request</div>
              </div>
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default Search;
