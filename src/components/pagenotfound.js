import React, { Component } from "react";

import "./pagenotfound.css";
import pnf from "../assets/404.png";

class PageNotFound extends Component {
  render() {
    return (
      <div className="pnf shift">
        <img src={pnf} alt="404" height={100} className="pnfimg" />
        <p className="pnftext">
          The page you are trying to reach doesn't exist!
        </p>
      </div>
    );
  }
}

export default PageNotFound;
