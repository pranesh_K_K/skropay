import React, { Component } from "react";
import { connect } from "react-redux";

import Search from "./search";
import "./home.css";


import sample from "../assets/icon-07.png";
import buyer from "../assets/buyer.png";
import seller from "../assets/seller.png";

class Home extends Component {
 constructor(props) {
    super(props);
    this.state = {
      recent: [
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
        { firstname: "Teja" },
      ],
      mode: null,
    };
  }
componentDidMount(){
}
  render() {
    return (
      <div className="home shift">
        <div className="grayspace">
          <h2>Start transactions with Skropay</h2>
          <p>Let us help you make the safest transaction</p>
        </div>
        <div className="homecomponents">
          <div className="buyerseller">
            {/*onClick Events*/}
            <div
              className={
                "bsele " + (this.state.mode === "Buyer" ? "bselected" : "")
              }
              onClick={() => this.setState({ mode: "Buyer" })}
            >
              <img src={buyer} height={80} alt="buyer" className="bsimg" />
              <div className="bsop">
                <label className="tnc">
                  I am a Buyer
                  <input
                    name="buyerseller"
                    id="buyer"
                    type="radio"
                    onChange={() => ""}
                    checked={this.state.mode === "Buyer"}
                  />{" "}
                  {/* Dumb but still giving empty onChange to avoid error of setting checked without onChange */}
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
            <div
              className={
                "bsele " + (this.state.mode === "Seller" ? "bselected" : "")
              }
              onClick={() => this.setState({ mode: "Seller" })}
            >
              <img src={seller} height={80} alt="seller" className="bsimg" />
              <div className="bsop">
                <label className="tnc">
                  I am a Seller
                  <input
                    name="buyerseller"
                    id="seller"
                    type="radio"
                    onChange={() => ""}
                    checked={this.state.mode === "Seller"}
                  />
                  <span className="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
          {this.state.mode && <Search mode={this.state.mode} />}
          {this.state.mode && (
            <div className="recentpayments">
              <div className="recenttitle" style={{ color: "#000000b6" }}>
                Recent Payments
              </div>
              <div className="recentmembers">
                {this.state.recent.map((info, index) => (
                  <div className="recent" key={"recentuser" + index}>
                    <img src={sample} alt="user" height={50} />
                    <p>{info.firstname}</p>
                  </div>
                ))}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.userToken,
  };
};

export default connect(mapStateToProps)(Home);
