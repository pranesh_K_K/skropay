import React, { Component } from 'react';
import { connect } from 'react-redux';

import './hold.css';

class Sold extends Component {
    render() {
        return (
            <div className="sold">
                <div className="sold-header">Sold</div>
                <div className="sold-users">
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                </div>
            </div>
        )
    }
}

class Bought extends Component {
    render() {
        return (
            <div className="bought">
                <div className="bought-header">Bought</div>
                <div className="sold-users">
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                    <div className="sold-user">
                        <div className="sold-user-details">
                            <div className="sold-user-icon"></div>
                            <div>
                                <div>Teja Pujari</div>
                                <div>#skro123</div>
                            </div>
                        </div>
                        <div className="sold-user-amount">₹200</div>
                    </div>
                </div>
            </div>
        )
    }
}

class Hold extends Component {
    constructor(props){
        super(props);
    }
    render() {
        return (
            <div className="shift">
                <h1>Skropay Hold</h1>
                <div className="hold">
                    <Sold />
                    <Bought />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        user: state.userToken,
    }
}

export default connect(mapStateToProps)(Hold)