import React, { Component } from "react";
import { connect } from "react-redux";
import "./wallet.css";

import icon from "../assets/icon-01.png";
import reward from "../assets/mastercard.svg";
import visa from "../assets/visa.svg";
import mc from "../assets/mastercard.svg";

class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "Teja",
      balance: "2000",
      rewards: [
        { company: "Netflix", desc: "30-day free trial", redeemed: true },
        {
          company: "Lenskart",
          desc: "Free Pair on next order",
          redeemed: false,
        },
        {
          company: "Bluedart",
          desc: "Free Shipping on next order",
          redeemed: false,
        },
        { company: "Zomato", desc: "Free Dips", redeemed: false },
        { company: "Foodpanda", desc: "Free Fries", redeemed: false },
      ],
      cards: [
        {
          company: "visa",
          cardnumber: "XXXX XXXX XXXX 1234",
          name: "Teja Pujari",
          exp: "09/25",
        },
        {
          company: "mc",
          cardnumber: "XXXX XXXX XXXX 5678",
          name: "Teja Pujari",
          exp: "09/22",
        },
        {
          company: "visa",
          cardnumber: "XXXX XXXX XXXX 4321",
          name: "Teja Pujari",
          exp: "12/24",
        },
        {
          company: "mc",
          cardnumber: "XXXX XXXX XXXX 8765",
          name: "Teja Pujari",
          exp: "05/23",
        },
      ],
      transactions: [
        { type: "add", amount: "2000" },
        { type: "paid", person: "Teja Pujari", amount: "2500" },
        { type: "received", person: "Teja Pujari", amount: "3500" },
        { type: "withdraw", amount: "4200" },
        { type: "add", amount: "3000" },
        { type: "withdraw", amount: "3000" },
      ],
    };
  }

  txString = (x, person = "") => {
    switch (x) {
      case "add":
        return <p>Money added to Wallet</p>;
      case "paid":
        return (
          <p>
            Paid to <strong>{person}</strong>
          </p>
        );
      case "received":
        return (
          <p>
            Received from <strong>{person}</strong>
          </p>
        );
      case "withdraw":
        return "Money withdrawn from Wallet";
      default:
        return "";
    }
  };

  render() {
    return (
      <div className="wallet shift">
        <div className="walfirst">
          <div className="greet">
            <h1>Hello, {this.state.firstname}!</h1>
            <p>
              Good{" "}
              {new Date().getHours() < 12
                ? "Morning"
                : new Date().getHours() < 18
                  ? "Afternoon"
                  : "Evening"}
              !
            </p>
          </div>
          <div className="wal-graph">
            {" "}
            {/*Wallet Balance and Graph */}
            <div className="wal">
              <p className="walbaltitle">Wallet Balance</p>
              <p className="walbal">₹{this.state.balance}</p>
              <p className="date">
                {new Date().toLocaleDateString("en-US", {
                  weekday: "long",
                  year: "numeric",
                  month: "long",
                  day: "numeric",
                })}
              </p>
            </div>
            <div className="graph"></div>
          </div>
          <div className="waladd-withdraw">
            <div className="waladd">
              <p className="waladdtitle">Add Money</p>
              <div className="walwrap">
                <div className="walfield">
                  <input
                    type="text"
                    value="₹"
                    className="curr-symbol"
                    disabled
                  />
                  <input
                    type="number"
                    placeholder="Enter Amount"
                    min="0"
                    className="amount"
                  />
                </div>
                <button className="waladdbtn">Proceed</button>
              </div>
            </div>
            {/*Add Money */}
            <div className="walwithdraw">
              <p className="walwithdrawtitle">Withdraw Money</p>
              <div className="walwrap">
                <div className="walfield">
                  <input
                    type="text"
                    value="₹"
                    className="curr-symbol"
                    disabled
                  />
                  <input
                    type="number"
                    placeholder="Enter Amount"
                    min="0"
                    className="amount"
                  />
                </div>
                <button className="walwithdrawbtn">Proceed</button>
              </div>
            </div>
            {/*Withdraw Money */}
          </div>
          <p className="rewardstitle">Rewards</p>
          <div className="rewards">
            {this.state.rewards.map((info, index) => (
              <div className="reward" key={"reward" + index}>
                <p className="rewardcomp">{info.company}</p>
                <img
                  src={reward}
                  className="rewardlogo"
                  alt="rewardlogo"
                  height={40}
                />
                <p className="rewarddesc">{info.desc}</p>
                <p className={info.redeemed ? "redeemed" : "unredeemed"}>
                  {info.redeemed ? "Redeemed" : "Redeem"}
                </p>
              </div>
            ))}
          </div>
          {/*Rewards*/}
        </div>
        <div className="walsecond">
          <div className="cards">
            <p className="saved">Saved Cards</p>
            {this.state.cards.map((info, index) => (
              <div className="card" key={"card" + index}>
                <img
                  src={info.company === "visa" ? visa : mc}
                  alt="cardlogo"
                  className="cardlogo"
                  height={info.company === "visa" ? 20 : 25}
                />
                <p className="cardnumber">{info.cardnumber}</p>
                <div className="carddetails">
                  <p className="cardowner">{info.name}</p>
                  <p className="cardexp">{info.exp}</p>
                </div>
              </div>
            ))}
          </div>
          <div className="history">
            <div className="history-header">All Transaction History</div>
            <div className="transactions">
              {this.state.transactions.map((info, index) => (
                <div className="transaction">
                  <img
                    src={icon}
                    alt="tx-icon"
                    height={45}
                    className="txicon"
                  />
                  <div className="txmini">
                    {this.txString(info.type, info.person)}
                    <p className="txamount">₹{info.amount}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.userToken,
  };
};

export default connect(mapStateToProps)(Wallet);
