import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import {
  Container,
  Row,
  Col,
  Table,
  OverlayTrigger,
  Tooltip,
  Button,
  Modal
} from "react-bootstrap";
import seller from "../assets/user.png";
import "./account.css";
import "./home.css";
import copy from "../assets/copy.png";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tooltip: "copy to clipboard",
      email: this.props.email,
      password: "",
      imageUrl:this.props.profile,
      first_name: this.props.firstname,
      last_name: this.props.lastname,
      profile_img:"",
      phone_number: this.props.mobile,
      edit:[false,false,false,false,false],
      username:this.props.username,
      show:false
    };
    this.upload=this.upload.bind(this);
    this.handleChange=this.handleChange.bind(this);
  }
  componentDidMount() {}
  renderTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
      {this.state.tooltip}
    </Tooltip>
  );
  handleChange(e){
    e.preventDefault();
    this.setState({[e.target.name]:e.target.value});
  }
  componentDidUpdate(prevProps) {
    // Typical usage (don't forget to compare props):
    if (this.props.username !== prevProps.username) {
      this.setState({email: this.props.email,imageUrl:this.props.profile,first_name: this.props.firstname,last_name: this.props.lastname,phone_number: this.props.mobile,username:this.props.username,});
    }
  }
  save(e,field){
    e.preventDefault();
    this.setState({ error: false });
    var formData = new FormData();
    formData.append([field], this.state[field]);
    // formData.append("phone_number", this.state.phone);
    // formData.append("first_name", this.state.firstname);
    // formData.append("last_name", this.state.lastname);
    // formData.append("email", this.state.email);
    // formData.append("password", this.state.password);
    // formData.append("username", this.state.firstname +" "+ this.state.lastname);
    // formData.append("profile_image", this.state.imageUrl);
    axios
      .put(
        "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/user/"+`${this.props.skyropay_id}`+"/",
        formData
      )
      .then((response) => {
       
      })
      .catch((error) => {
        //handle error
        this.setState({
          errorwith: Object.keys(error.response.data),
          error: true,
          warning: "User Already Exists.",
        });
      });
  }
  upload(e) {
    const [file] = e.target.files
    if (file) {
     this.setState({imageUrl:URL.createObjectURL(file),profile_img:e.target.files[0]})
    }
  }
  render() {
    return (
      <div className="home shift">
        <Container style={{ padding: "50px" }}>
          <Row style={{ paddingTop: "50px" }}>
            <Col sm={4} md={1}>
              <img src={this.state.imageUrl?this.state.imageUrl:seller}
              onError={()=>{this.setState({imageUrl:seller})}} width={65} height={55} alt="home" />
               <div style={{width:"65px"}} className="button-wrapp">
              <span className="label">
                Edit
              </span>
                <input type="file" name="upload" id="upload" onChange={this.upload} className="upload-box" placeholder="Profile Upload"/>
            </div>
            </Col>
            <Col>
              <h5 style={{ opacity: 0.9 }}>{this.props.username}</h5>
              <p style={{ paddingTop: "10px", fontSize: "15px", opacity: 0.7 }}>
                Skyropay ID: {this.props.skyropay_id}
                <OverlayTrigger
                  placement="right"
                  delay={{ show: 250, hide: 400 }}
                  overlay={this.renderTooltip}
                >
                  <button style={{ border: "none", background: "none" }}>
                    <img
                      style={{ padding: "2px" }}
                      onClick={() => {
                        this.setState({ tooltip: "copied" });
                        setTimeout(
                          this.setState({ tooltip: "copy to clipboard" }),
                          5000
                        );
                        navigator.clipboard.writeText(this.props.skyropay_id);
                      }}
                      src={copy}
                      height={14}
                    ></img>
                  </button>
                </OverlayTrigger>
              </p>
            </Col>
          </Row>
          <Row style={{ paddingTop: "50px" }}>
            <Col sm={12}>Account Setting</Col>
          </Row>
          <Row style={{ paddingTop: "50px",opacity:0.8 }}>
            <Col sm={2}>Frist Name</Col>
            <Col sm={8}>{this.state.edit[0]?<div className="searchbox"> <input
                type="text"
                name="first_name"
                onChange={this.handleChange}
                value={this.state.first_name}
                placeholder="User Name"
                required
              /> <Button variant="secondary" onClick={(event)=>this.save(event,"first_name")} style={{backgroundColor:"#336cf9",outline:0,border:"none"}} size="sm">
              save
            </Button></div>:this.state.first_name}</Col>
            <Col sm={2} onClick={()=>this.setState({edit:{[0]:true}})} style={{color:"#336cf9",cursor: "pointer"}}>Edit</Col>
          </Row>
          <Row style={{ paddingTop: "50px",opacity:0.8 }}>
            <Col sm={2}>Last Name</Col>
            <Col sm={8}>{this.state.edit[1]?<div className="searchbox"> <input
                type="text"
                name="last_name"
                onChange={this.handleChange}
                value={this.state.last_name}
                placeholder="Last Name"
                required
              /> <Button variant="secondary" onClick={(event)=>this.save(event,"last_name")} style={{backgroundColor:"#336cf9",outline:0,border:"none"}} size="sm">
              save
            </Button></div>:this.state.last_name}</Col>
            <Col sm={2} onClick={()=>this.setState({edit:{[1]:true}})} style={{color:"#336cf9",cursor: "pointer"}}>Edit</Col>
          </Row>
          <Row style={{ paddingTop: "50px",opacity:0.8 }}>
            <Col sm={2}>Phone number</Col>
            <Col sm={8}>{this.state.edit[2]?<div className="searchbox"> <input
                type="text"
                name="phone_number"
                onChange={this.handleChange}
                value={this.state.phone_number}
                placeholder="Phone Number"
                required
              /><Button variant="secondary" onClick={(event)=>this.save(event,"phone_number")} style={{backgroundColor:"#336cf9",outline:0,border:"none"}} size="sm">
              save
            </Button></div>:this.state.phone_number}</Col>
            <Col sm={2} onClick={()=>this.setState({edit:{[2]:true}})} style={{color:"#336cf9",cursor: "pointer"}}>Edit</Col>
          </Row>
          <Row style={{ paddingTop: "50px",opacity:0.8 }}>
            <Col sm={2}>Email address</Col>
            <Col sm={8}>{this.state.edit[3]?<div className="searchbox"> <input
                type="text"
                name="email"
                onChange={this.handleChange}
                value={this.state.email}
                placeholder="Email"
                required
              /><Button variant="secondary" onClick={(event)=>this.save(event,"email")} style={{backgroundColor:"#336cf9",outline:0,border:"none"}} size="sm">
              save
            </Button></div>:this.state.email}</Col>
            <Col sm={2} onClick={()=>this.setState({edit:{[3]:true}})} style={{color:"#336cf9",cursor: "pointer"}}>Edit</Col>
          </Row>
          <Row style={{ paddingTop: "50px",opacity:0.8 }}>
            <Col sm={2}>Password</Col>
            <Col sm={8}>{this.state.edit[4]?<div className="searchbox"> <input
                type="password"
                name="password"
                onChange={this.handleChange}
                value={this.state.password}
                placeholder="Password"
                required
              /><Button variant="secondary" onClick={(event)=>this.save(event,"password")} style={{backgroundColor:"#336cf9",outline:0,border:"none"}} size="sm">
              save
            </Button></div>:<>&#9679; &#9679; &#9679; &#9679; &#9679; &#9679; &#9679;</>}</Col>
            <Col sm={2} onClick={()=>this.setState({edit:{[4]:true}})} style={{color:"#336cf9",cursor: "pointer"}}>Edit</Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => {

  console.log(state);
  return {
    auth: state.auth,
    user: state.userToken,
    username: state.currentUser.user.username,
    email: state.currentUser.user.email,
    mobile: state.currentUser.user.phone_number,
    profile: state.currentUser.user.profile_image,
    skyropay_id: state.currentUser.user.id,
    firstname: state.currentUser.user.first_name,
    lastname: state.currentUser.user.last_name
  };
};

export default connect(mapStateToProps)(Home);
