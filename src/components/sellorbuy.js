import React, { Component } from 'react';
import { connect } from 'react-redux';

import './home.css';

class Home extends Component {
    render() {
        return (
            <div className="home shift">
                <h1>Welcome to Skropay!</h1>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth,
        user: state.userToken,
    }
}

export default connect(mapStateToProps)(Home)