import React, {
  Component
} from "react";
import axios from "axios";
import Cookies from "js-cookie";
import {
  Link
} from "react-router-dom";
import {
  connect
} from "react-redux";
import {
  setTokenHeader
} from "../services/service";
import {
  SET_CURRENT_USER
} from "../store/actiontype";
import jwtDecode from "jwt-decode";
import "./login.css";
import logo from "../assets/logo.png";

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      emailorphone: "",
      password: "",
      currpage: 0,
      error: false,
      warning: "No warning yet.",
    };
  }

  handleInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  submitLogin = (e) => {
    e.preventDefault();
    this.setState({
      error: false,
      warning: "No warning"
    });

    var formData = new FormData();
    formData.append("email", this.state.emailorphone);
    formData.append("password", this.state.password);

    new Promise((resolve, reject) => axios
      .post(
        "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/token/",
        formData, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
        }
      )
      .then(async (response) => {
        //handle success
        const {
          refresh,
          access
        } = response.data;
        
        Cookies.set("access", access);
        Cookies.set("refresh", refresh);
        setTokenHeader(access);
        localStorage.setItem("token", access);
        
        new Promise((resolve, reject) => axios
      .get(
        "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/user/"+`${jwtDecode(access).user_id}`+"/",
        formData, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
        }
      )
      .then(async (data) => {
      resolve(this.props.dispatch({
        type: SET_CURRENT_USER,
        data:data.data
      }));
      })).catch(() => {
        const data = response.data;
        resolve(this.props.dispatch({
          type: SET_CURRENT_USER,
          data
        }));
      })
      })
      .catch((error) => {
        //handle error
        reject(this.setState({
          error: true,
          warning: "Wrong password. Try again or click 'Forgot password' to reset it.",
        }));
      }));
  };

  pageIncrement = () => {
    this.setState(({
      currpage
    }) => {
      return {
        currpage: currpage + 1
      };
    });
  };

  pageDecrement = () => {
    this.setState(({
      currpage
    }) => {
      return {
        currpage: currpage - 1
      };
    });
  };

  render() {
    return (
      <div className="login">
        <div className="logreglogo">
          <img src={logo} alt="logo" height={40} />
        </div>
        {this.state.currpage === 0 && (
          <div className="login-1">
            <div className="maxlogregwidth">
              <div className="login-title">Login to your Skropay Account</div>
              <input
                className="fw skro-input"
                type="text"
                name="emailorphone"
                placeholder="Email or Phone Number"
                value={this.state.emailorphone}
                onChange={this.handleInput}
              />
              <input
                className="fw skro-input"
                type="password"
                name="password"
                placeholder="Password"
                onChange={this.handleInput}
                autoComplete="new-password"
              />
              <div className="login-2-2-2 click" onClick={this.pageDecrement}>
                Forgot Password?
              </div>
              <p
                className="warning noselect"
                style={{ color: this.state.error ? "red" : "white" }}
              >
                {this.state.warning}
              </p>
              <button className="skro-btn-3" onClick={this.submitLogin}>
                Login
              </button>
            </div>
            <div className="space-1"></div>
            <div className="alignc">
              New User? <Link to="/register">Register</Link>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.userToken,
  };
};

export default connect(mapStateToProps)(Login);
