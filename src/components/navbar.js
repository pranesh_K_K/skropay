import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { SET_CURRENT_USER } from "../store/actiontype";
import "./navbar.css";
import Cookies from "js-cookie";
import seller from "../assets/user.png";
import logo from "../assets/logo.png";

const NavigationBar = withRouter((props) => <Navigationbar {...props} />);

class Navigationbar extends Component {
  state={
    image:""
  }
  render() {
    if (["/login", "/register"].includes(this.props.location.pathname)) {
      return null;
    }
    return (
      <Navbar
        expand="lg"
        className={
          ["/login", "/register"].includes(this.props.location.pathname)
            ? "hide"
            : ""
        }
      >
        <Navbar.Brand href="/">
          <img src={logo} height={40} alt={logo} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <p style={{paddingTop:"10px"}}>{this.props.username}</p>
            {/*<Nav.Link href="/" className={(this.props.location.pathname === '/') ? 'nav-active-link' : ''}>Home</Nav.Link>
                        <Nav.Link href="/about" className={(this.props.location.pathname === '/about') ? 'nav-active-link' : ''}>About</Nav.Link>
                        <Nav.Link href="/terms-and-conditions" className={(this.props.location.pathname === '/terms-and-conditions') ? 'nav-active-link' : ''}>Policies</Nav.Link>
                        <Nav.Link href="/faqs" className={(this.props.location.pathname === '/faqs') ? 'nav-active-link' : ''}>FAQs</Nav.Link>
                        <Nav.Link href="/support" className={(this.props.location.pathname === '/support') ? 'nav-active-link' : ''}>Support</Nav.Link>*/}
            <NavDropdown title={ <img
              src={this.state.image?this.state.image:this.props.profile?this.props.profile:seller}
              onError={()=>{this.setState({image:seller})}}
              height={35}
              alt="home"
            />} >
          <NavDropdown.Item href={"/login"}  onClick={()=>{ localStorage.clear();
                this.props.dispatch({
                  type: SET_CURRENT_USER,
                  ...{}
                });
                Cookies.remove('access');
                Cookies.remove('remove');}}>Logout</NavDropdown.Item>
        </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.currentUser.isAuthenticated,
    username: state.currentUser.user.username,
    user: state.userToken,
    profile: state.currentUser.user.profile_image
  };
};

export default connect(mapStateToProps)(NavigationBar);
