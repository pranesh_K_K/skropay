import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./sidenav.css";
import homeicon from "../assets/icon-01.png";
import dealsicon from "../assets/icon-02.png";
import walleticon from "../assets/icon-03.png";
import holdicon from "../assets/icon-04.png";
import addicon from "../assets/icon-05.png";
import withdrawicon from "../assets/icon-06.png";
import historyicon from "../assets/icon-07.png";
import seller from "../assets/user.png";

const SideNav = withRouter((props) => <Sidenav {...props} />);

class Sidenav extends Component {
  state={
    image:""
  }
  render() {
    if (["/login", "/register"].includes(this.props.location.pathname)) {
      return null;
    }
    return (
      <div className="sidenav">
        <div className="actions">
          <Link
            to="/home"
            className={
              "action" +
              (this.props.location.pathname === "/home" ? " action-active" : "")
            }
          >
            <img
              src={homeicon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>Home</p>
          </Link>
          <Link
            to="/deals"
            className={
              "action" +
              (this.props.location.pathname === "/deals"
                ? " action-active"
                : "")
            }
          >
            <img
              src={dealsicon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>Deals</p>
          </Link>
          <Link
            to="/wallet"
            className={
              "action" +
              (this.props.location.pathname === "/wallet"
                ? " action-active"
                : "")
            }
          >
            <img
              src={walleticon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>Wallet</p>
          </Link>
          <Link
            to="/hold"
            className={
              "action" +
              (this.props.location.pathname === "/hold" ? " action-active" : "")
            }
          >
            <img
              src={holdicon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>
              Skropay
              <br /> Hold
            </p>
          </Link>
          <Link
            to="/add"
            className={
              "action" +
              (this.props.location.pathname === "/add" ? " action-active" : "")
            }
          >
            <img src={addicon} height={45} alt="home" className="action-icon" />
            <p>Add Money</p>
          </Link>
          <Link
            to="/withdraw"
            className={
              "action" +
              (this.props.location.pathname === "/withdraw"
                ? " action-active"
                : "")
            }
          >
            <img
              src={withdrawicon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>Withdraw</p>
          </Link>
          <Link
            to="/history"
            className={
              "action" +
              (this.props.location.pathname === "/history"
                ? " action-active"
                : "")
            }
          >
            <img
              src={historyicon}
              height={45}
              alt="home"
              className="action-icon"
            />
            <p>
              Transaction
              <br /> History
            </p>
          </Link>
          <Link
            to="/account"
            className={
              "action" +
              (this.props.location.pathname === "/account"
                ? " action-active"
                : "")
            }
          >
            <img
              src={this.state.image?this.state.image:this.props.profile?this.props.profile:seller}
              onError={()=>{this.setState({image:seller})}}
              height={35}
              alt="home"
              className="action-icon"
            />
            <p>
              Account
            </p>
          </Link>
        </div>
      </div> /*hehe */
    );
  }
}
const mapStateToProps=state=>{
  return ({
    profile:state.currentUser.user.profile_image
  })
}
export default connect(mapStateToProps)(SideNav);
