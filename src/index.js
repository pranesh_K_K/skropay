import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import rootReducer from './store/reducers';
import axios from "axios";
import jwtDecode from "jwt-decode";
import { SET_CURRENT_USER } from "./store/actiontype";
import {  setTokenHeader } from "./services/service";
import { createStore } from 'redux';
import { Provider } from 'react-redux';

const store = createStore(rootReducer)

if (localStorage.token) {
  try {
    setTokenHeader(localStorage.token);
    new Promise((resolve, reject) => axios
    .get(
      "http://skropay-env.ap-south-1.elasticbeanstalk.com/api/usermgmt/user/"+`${jwtDecode(localStorage.token).user_id}`+"/"
    )
    .then(async (data) => {
    resolve(store.dispatch({
      type: SET_CURRENT_USER,
      data:data.data
    }));
    }).catch(() => {
      resolve(store.dispatch({
        type: SET_CURRENT_USER,
        data:localStorage.token
      }));
    }))
    store.dispatch({
      type: SET_CURRENT_USER,
      data:localStorage.token
    });
  } catch (e) {
  }
}
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
