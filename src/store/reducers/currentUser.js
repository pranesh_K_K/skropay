import { SET_CURRENT_USER } from "../actiontype";

const DEFAULT_STATE = {
  isAuthenticated: false, // hopefully be true, when logged in
  user: {}, // all the user info when logged in
};

export default (state = DEFAULT_STATE, action) => {
  console.log(action);
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        // turn empty object into false or if there are keys, true
        isAuthenticated: action.data?!!Object.keys(action.data).length:false,
        user: action.data,
      };
    default:
      return state;
  }
};
